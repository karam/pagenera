#!/bin/sh -e

##
## This is an example make.sh file for the HTML example
##


# Assure the script is running in the directory that it exists in

test "$(readlink -f "$(pwd)")" = "$(readlink -f "$(dirname $0)")"


# Create and/or clean OUTPUT, and cd into it

mkdir -p OUTPUT
rm -rf OUTPUT/*
cd OUTPUT


# Run Pagenera

PAGENERA_GLOBAL_FILENAME='__global.html' \
	PAGENERA_OUTPUT_SUFFIX= \
	PAGENERA_PASTE_OPTIONS=SHADOW:DELIMIT:INDENT \
	pagenera ../template.html ../articles ../about.html


# Get back to the parent directory

cd ..

