#!/usr/bin/env python3
#
# Copyright (C) 2021 Karam Assany <karam.assany@disroot.org>
#
# This file is part of Pagenera.
#
# Pagenera is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pagenera is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pagenera.  If not, see <https://www.gnu.org/licenses/>.
#
#
# For hackers:
#
# The source code strictly adheres to the specification in pagenera(1).  If you
#   want to change, add or remove a feature, do it in pagenera(1) first.
#
# Strings that spread beyond the allowed width (80 characters) are separated by
#   by a concatenation (+) operator.  This is done to keep the indentication
#   intact.
#
# Single quotation is used for internal strings, while double quotation is used
#   for printed strings (i.e. stderr output).
#
# The following identifier naming style is utilized:
#   UPPER_CASE_STYLE  for constants
#   lower_case_style  for variables
#   camelCaseStyle    for functions
#   TitleCaseStyle    for classes
#


import sys, re, os



## Default options dictionary (see ENVIRONMENT in pagenera(1))

# You MAY edit this dictionary as a global configuration.
# Don't worry, even default values are to be validated.

DEFAULT_ENV = {
    'CRAWLING_ORDER' : 'ASCEND',
    'GLOBAL_FILENAME': 'global',
    'OUTPUT_SUFFIX'  : '.out',
    'PASTE_OPTIONS'  : '',
    'SLOT_REGEX'     : r'%[A-Z0-9\-]+%',
}


## Header and footer messages

# Keep the newlines the way they are

MSG_WELCOME = """Pagenera - a slot-and-filler text processor
Current version: 1.1 (2021-02-09)
Copyright (C) 2021 Karam Assany <karam.assany@disroot.org>
Source code: <https://codeberg.org/karam/pagenera>
Licensed under GNU GPLv3+.  See COPYING

Pagenera is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""


# Logging tags; used with log()

TAG_NOTE  = "\033[0;32m" + "NOTE"  + "\033[m"
TAG_WARN  = "\033[0;33m" + "WARN"  + "\033[m"
TAG_ERROR = "\033[0;31m" + "ERROR" + "\033[m"
TAG_FATAL = "\033[1;31m" + "FATAL" + "\033[m"


## Error codes

ERR_ALRIGHT  = 0
ERR_USAGE    = 1
ERR_ENVIRON  = 2
ERR_TEMPLATE = 3


## Insider constants

# Prefix for environment variables
ENVIRON_PREFIX = 'PAGENERA_'

# Possible values for CRAWLING_ORDER and PASTE_OPTIONS
VALUES_CRAWLING_ORDER = ('ASCEND', 'DESCEND')
VALUES_PASTE_OPTIONS  = ('DELIMIT', 'EMPTY', 'INDENT', 'SHADOW')


# Insider global variables

env = DEFAULT_ENV

template_slots  = []  # dummy initial value
template_gapped = []  # dummy initial value


# Internal exception definitions

class IntSyntaxError  (Exception): pass
class IntEnvironError (Exception): pass
class IntTemplateError(Exception): pass
class IntBuildError   (Exception): pass



def log(tag, body):  # use TAG_* for the tag parameter

    if tag in (TAG_ERROR, TAG_FATAL): # reddy log for errors
        body = "\033[31m" + body + "\033[m"

    # "[tag] body" with bold brackets
    print("\033[1m[\033[m" + tag + "\033[1m]\033[m " + body, file=sys.stderr)



def extractStore(lines):

    store = {}
    open_slots = []

    for line in lines:

        if open_slots:  # i.e. if there is an open definition

            if line == '</_>\n':  # check for a definition footer
                open_slots = []
            else:
                # Append the current line in the filler string
                for slot in open_slots:
                    store[slot] += line
        
        else:

            # Check for a definition header
            if line.startswith('<_ ') and line.endswith('>\n'):
                
                # Parse the list of whitespace-separated slots
                open_slots = line[3:-2].split()

                if not open_slots:
                    log(TAG_WARN, "Filler but for no slot at all")
        
                for slot in open_slots:

                    if slot not in template_slots:
                        log(TAG_WARN,
                            "Filler definition for an unknown slot: " + slot)
                        open_slots.remove(slot)
                    else:
                        store[slot] = ''  # initilize the filler string

    # Remove the trailing newline in the filler, if any
    for slot, filler in store.items():
        if filler:
            store[slot] = filler[:-1]

    # End of page yet a definition is open
    if open_slots:
        log(TAG_WARN, "Unclosed filler definition for: " +
                            ", ".join(open_definitions))

    # End of page yet the store is empty
    if not store:
        log(TAG_WARN, "Page with zero filler definitions")

    return store



def mergeStore(old_store, current_store):

    # I reckon this is the sole pure function in the whole program...

    new_store = old_store.copy()

    for slot, filler in current_store.items():
        new_store[slot] = filler

    return new_store



def compilePage(store):

    def getSlotBefore(index):  # return the left side of the line

        splitted = template_gapped[index].split('\n')

        return splitted[ len(splitted) - 1 ]


    def getSlotAfter(index):  # returns the right side of the line

        return template_gapped[index + 1].split('\n')[0]


    def getSlotIndent(index):  # returns the indent of the line
  
        indent = ''

        for char in tuple(getSlotBefore(index)):
            if char in tuple(' \t'):
                indent += char
            else:
                break

        return indent


    def isSlotInserted(index):

        return ( getSlotBefore(index).lstrip(' \t') or getSlotAfter(index) )


    def setDelimit(item):  # forces a multi-line filler into a single line

        fill = store[item].split('\n')

        for ind, line in enumerate(fill):
            fill[ind] = line.strip(' \t')

        return ' '.join(fill)


    def setIndent(item, indent):

        # Idents a whole filler (except the first line of it)

        fill = store[item].split('\n')

        for ind, line in enumerate(fill[1:]):
            fill[ind + 1] = indent + line

        return '\n'.join(fill)


    page = template_gapped.copy()


    for index, item in enumerate(template_slots):

        if item in store.keys():
        
            # if DELIMIT took action, there is no need for INDENT
            if (
                    ('DELIMIT' in env['PASTE_OPTIONS']) and
                    (isSlotInserted(index)) and
                    (store[item])
                    ):
                fill = setDelimit(item)
            
            elif (
                    ('INDENT' in env['PASTE_OPTIONS'])
                    and (getSlotIndent(index))
                    ):
                fill = setIndent(item, getSlotIndent(index))

            else:
                fill = store[item]        

        else:
        
            if 'EMPTY' in env['PASTE_OPTIONS']:
                fill = ''

            else:
                log(TAG_WARN, "Unsatisfied slot: " + item)
                fill = item


        # Insert a filler between each split (i.e. inside each gap)
        # should've been ( index * 2 - 1 ) if index started from 1 instead of 0
        page.insert(index * 2 + 1, fill)


    return ''.join(page)



def buildPage(page_path, dest_path, parent_store = {}):

    log(TAG_NOTE, "Compiling page: " + page_path)

    if not os.path.isfile(page_path):
        raise IntBuildError("Page does not exist or is not a regular file")

    with open(page_path) as page_data:
            page_lines = page_data.readlines()

    with open(dest_path, 'w') as dest:
        dest.write(
                compilePage(
                    mergeStore(parent_store,
                        extractStore(page_lines)))) # Lisp says Hi :)



def buildSite(site_path, dest_path, parent_store = {}):

    def cleanDir(dirc):

        for fn in os.listdir(dirc):

            fp = os.path.join(dirc, fn)

            if os.path.islink(fp):  # a symlink
                os.unlink(fp)
            elif os.path.isdir(fp):
                cleanDir(fp)  # why use shutil.rmtree when we have recursion ;)
                os.rmdir(fp)
            else:  # assuming it's a regular file
                os.unlink(fp)


    log(TAG_NOTE, "Crawling site: " + site_path)

    pages = []
    subsites = []
    site_store = {}  # site-level store to be inherited by pages and subsites

    items = os.listdir(site_path)

    items.sort(reverse = (env['CRAWLING_ORDER'] == 'DESCEND'))

    for item in os.listdir(site_path):

        abs_item = os.path.join(site_path, item)  # absolute path for item

        if os.path.isfile(abs_item):

            if item == env['GLOBAL_FILENAME']:  # the site-level page
                log(TAG_NOTE, "Extracting site-level page: " + abs_item)
                with open(abs_item) as global_data:
                    site_store = mergeStore(parent_store,
                            extractStore(global_data.readlines()))
            else:
                pages.append(item)  # a normal page

        elif os.path.isdir(abs_item):
            subsites.append(item)  # a subsite

        else:
            raise IntBuildError(f"Item '{item}' is neither a regular file nor "
                    + "a directory")

    # If no site-level page exists
    if not site_store:
        site_store = parent_store.copy()


    # Warn for empty sites
    if not (pages + subsites):
        log(TAG_WARN, "Site with zero pages/subsites")

    # Clean the destination
    if os.path.isdir(dest_path):
        cleanDir(dest_path)
    else:
        os.mkdir(dest_path)

    # Build pages first
    for page in pages:
        buildPage(os.path.join(site_path, page), os.path.join(dest_path, page),
                site_store)

    # Subsites second
    for site in subsites:
        buildSite(os.path.join(site_path, site), os.path.join(dest_path, site),
                site_store)



def buildTarget(target_path):

    # Evaluating dest_path from target_path, cwd, and OUTPUT_SUFFIX
    dest_path = ( os.path.join(os.getcwd(), os.path.basename(target_path)) +
                    env['OUTPUT_SUFFIX'] )

    # Sanity check to prevent overwriting the target itself
    if os.path.realpath(target_path) == os.path.realpath(dest_path):
        raise IntBuildError(f"Will not build '{target_path}': Both source and "
                    + "destination point to the same place. Are you drunk?")


    if os.path.isfile(target_path):  # single page target

        log(TAG_NOTE, "Building single page target: " + target_path)
        buildPage(target_path, dest_path)


    elif os.path.isdir(target_path):  # site target

        log(TAG_NOTE, "Building site target: " + target_path)
        buildSite(target_path, dest_path)


    else:

        raise IntBuildError(f"Will not build '{target_path}': it does not "
                + "exist or is neither a regular file nor a directory")



def main():

    def isFilename(filename): # i.e. filename in Unix terms

        return not ( filename.count('/') + filename.count('\0') )


    # Global variables

    global template_slots
    global template_gapped


    # Welcome message

    print(MSG_WELCOME, file=sys.stderr)


    # Enable colors for NT platform

    if os.name == 'nt':
        os.system('color')


    # Validate commad-line syntax

    try:

        if len(sys.argv) == 1:
            raise IntSyntaxError("No template is given")

        elif len(sys.argv) == 2:
            raise IntSyntaxError("At least one target is required")

    except IntSyntaxError as e:

        log(TAG_FATAL, "Syntax error: " + e.args[0])
        log(TAG_FATAL, "Refer to the proper command syntax in paganera(1)")
        exit(ERR_USAGE)


    # Load environmnet variables, parse and validate options

    try:

        for var in env.keys():

            # Override default options by environment variables if they exist
            ext_var = ENVIRON_PREFIX + var
            if ext_var in os.environ.keys():
                env[var] = os.environ[ext_var]


            if False: 0 / 0  # switch statement emulation

            elif var == 'CRAWLING_ORDER':
                if env[var] not in VALUES_CRAWLING_ORDER:
                    raise IntEnvironError(ext_var, env[var], "One of: " +
                                ", ".join(VALUES_CRAWLING_ORDER))

            elif var == 'GLOBAL_FILENAME':
                if not env[var]:
                    raise IntEnvironError(ext_var, "is empty",
                                "any NON-EMPTY filename")
                if not isFilename(env[var]):
                    raise IntEnvironError(ext_var, "is not a filename",
                                "any non-empty FILENAME")

            elif var == 'OUTPUT_SUFFIX':
                if not isFilename(env[var]):
                    raise IntEnvironError(ext_var, "any filename")

            elif var == 'PASTE_OPTIONS':
                env[var] = env[var].split(':')  # parsing the combination
                for item in env[var]:
                    if item not in VALUES_PASTE_OPTIONS + ('',):
                        raise IntEnvironError(ext_var, item,
                                "An optional colon-seperated combination of: " +
                                ", ".join(VALUES_PASTE_OPTIONS))

            elif var == 'SLOT_REGEX':
                prohibited = ''
                for char in tuple(' \t\n()'):  # prohibited characters
                    if env[var].count(char):
                        prohibited += char

                if prohibited:
                    raise IntEnviron(ext_var, repr(prohibited),
                            "Any regex that does not contain whitespace or " +
                            "parentheses")
                env[var] = re.compile(env[var])

            else: 0 / 0  # strict validation

    except IntEnvironError as e:

        log(TAG_FATAL,
            "The environment variable {} contains illegal value: {}".format(
                                    e.args[0], e.args[1]))
        log(TAG_FATAL, "Possible values: " + e.args[2])
        exit(ERR_ENVIRON)



    # Define arguments

    template_path = sys.argv[1]
    target_paths  = sys.argv[2:]


    # Process the template

    log(TAG_NOTE, "Parsing the template: " + template_path)

    try:

        if not os.path.isfile(template_path):
            raise IntTemplateError("Template file does not exist or is not a " +
                    "regular file")
            exit(ERR_TEMPLATE)

        with open(template_path) as template_file:
            template_stream = template_file.read()

        # This creates a gapped template stream and an ordered list of
        # corresponding slots
        template_gapped = re.split  (env['SLOT_REGEX'], template_stream)
        template_slots  = re.findall(env['SLOT_REGEX'], template_stream)

        if not template_slots:
            log(TAG_WARN, "There are zero slots in the template")

    except IntTemplateError as e:

        log(TAG_FATAL, "Cannot parse the template: " + e.args[0])
        exit(ERR_TEMPLATE)


    # Process every target

    failure_count = ERR_ALRIGHT

    for target in target_paths:
    
        try:
            buildTarget(target)
        
        except IntBuildError as e:
            log(TAG_ERROR, "Failed to build target: " + target)
            log(TAG_ERROR, "Details: " + e.args[0])
            failure_count += 1


    # Finally, report

    log(TAG_NOTE, "Report: {} of {} targets were successful"
            .format(len(target_paths) - failure_count, len(target_paths)))

    if failure_count:
        status = failure_count + ERR_TEMPLATE
        if status > 255:  # avoid exit status overlapping
            status = 255
        exit(status)
    else:
        exit(ERR_ALRIGHT)



if __name__=='__main__': main()
