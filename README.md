# Pagenera


## Introduction

**Pagenera** (<em>Page</em> <em>Genera</em>tor For This <em>Era</em>) is a
slot-and-filler text processor that can compile individual text files or
hiercharies of such files starting from a general gapped template and
target-specific fillers.

It is very useful for templating HTML pages for static websites (the developer
made **Pagenera** particularly for this purpose).  You may find out other
interesting use-cases, and we hope you will report them back to us to include
them as examples.

**Pagenera** is very simple in terms of program complexity, and it is
well-documented.  It also has a specification, which is is melt in with the
documentation to provide a single reference.

Currently, **Pagenera** is implemented in _Python 3_ without external
dependencies.


## Installation

We assume your Python 3 implementation can be reached at `/usr/bin/env python3`.

```sh
P=pagenera # lazy shortcut
D=/usr/local # installation place
install $P.py $D/bin/
gzip -fkn9 $P.1 && install -m644 $P.1.gz $D/share/man/man1/ && rm -f $P.1.gz
```


## Documentation

Refer to the `pagenera(1)` manual page.

As a starter, you may be interested in some basic real-life examples which are
included in the [examples/](./examples) directory.


## License

> Copyright (C) 2021 Karam Assany (karam.assany@disroot.org)
>
> Pagenera is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
>
> Pagenera is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with Pagenera.  If not, see <https://www.gnu.org/licenses/>.
