.\""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.\" Copyright (C) 2021 Karam Assany <karam.assany@disroot.org>
.\"
.\" This file is part of Pagenera.
.\"
.\" Pagenera is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 3 of the License, or
.\" (at your option) any later version.
.\"
.\" Pagenera is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Pagenera.  If not, see <https://www.gnu.org/licenses/>.
.\""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
.\"
.TH PAGENERA 1 "2021-02-09" "version 1.1" "Pagenera Documentation"
.\"
.\"
.SH NAME
pagenera \- a slot-and-filler text processor
.\"
.\"
.SH SYNOPSIS
.B pagenera
.I <template> <target1>
[\fI<target2>\fR ...]
.\"
.\"
.SH DESCRIPTION
.B Pagenera
(\fIPage Genera\fRtor For This \fIEra\fR) is a slot-and-filler text processor
that can compile individual text files (called \fIpages\fR) or hiercharies of
such files (called \fIsites\fR) starting from a general gapped template and
target-specific fillers.
.TP
.B <template>
is the path to the template, which MUST be a text regular file.
.TP
.B <targetN>
is the path to a target, which MUST be either an single
.I page
or a \fIsite\fR (see below).
.\"
.\"
.SH PAGES AND SITES
Pages MUST be text regular files (just like the template), and sites MUST be
directories.  Sites MAY also contain sub-sites (sub-directories).  Sites MUST
NOT contain files other than pages as well as sub-sites.
.PP
Moreover, pages SHOULD contain at least one \fIfiller\fR definition, and sites
SHOULD contain at least one page (or one subsite).  The template SHOULD contain
at least one \fIslot\fR.
.\"
.SS Notes
For a single page (resp. site) as an input, \fB<targetN>\fR, the output will be
written in a text regular file (resp. directory) called \fB<targetN>.out\fR
inside the current working directory (regardless of the location of the input).
If such file exists,
.B Pagenera
may
.I silently
empty it before the compilation.  You can change the output suffix from
.B .out
to something else by setting the
.I PAGENERA_OUTPUT_SUFFIX
environment variable.  The suffix MAY be an empty string, if not, it MUST be a
valid filename in Unix terms.  Apparently,
.B Pagenera
will assure that the output will never overwrite the input.
.PP
Also beware that within sites, pages whose name is
.B global
(by default) are recognized as \fIsite-level pages\fR.  See the
.B COMPILATION
section for more.
.\"
.\"
.SH SLOTS
Slots are strings that exist within the template file which match the regex
.I %[A-Z0-9\-]+%
(by default).  You can change the regex to something else by setting the
.I PAGENERA_SLOT_REGEX
environment variable.  Beware that Python's \fBre\fR module is used for parsing
and processing the regex.
.\"
.SS Notes
There is generally no restriction on the length or position of slots; as long as
they match the given regex.
.PP
Parentheses, whitespace (spaces, tabs and newlines) MUST NOT be used in the
regex.
.\"
.SS Example (HTML)
<html lang="\fI%LANG%\fR">
.br
<title>\fI%TITLE%\fR</title>
.br
<a href="https://abc.xyz/\fI%LANG%\fR/index.html">Index</a>
.br
<article>
.br
.I \t%NOTIF-3%
.br
</article>
.br
</html>
.\"
.\"
.SH FILLERS
Fillers are pieces of text that exist inside a filler definition.  Filler
definitions SHOULD exist in every page.  Each filler definition can cover a
slot or more.  Fillers MAY be re-defined (i.e. overridden by more recent filler
definition).  See the
.B COMPILATION
section to understand how, and in which chronological order, filler definitions
are evaluated by \fBPaganera\fR.
.\"
.SS Syntax
\fB<_\fR \fISLOT1 SLOT2\fR ... \fISLOTN\fR\fB>\fR
.br
Filler
.br
.B </_>
.PP
Note that the definition header and footer MUST be on their single lines, and
MUST NOT contain leading or trailing whitespace except the line terminator, as
well as the whitespace separating slots inside the header of the definition.
.PP
The filler part MAY expand through multiple lines, and it MAY be entirely
omitted as well (an empty filler).
.\"
.SS Note
Currently, the way fillers are extracted (definition syntax etc) is very
specific to \fBPagenera\fR, and there is no plan to generalize it.  If you've
got some fascinating idea about that, you're always welcome to contribute it :)
.\"
.\"
.SH COMPILATION
The compilation process can be divided into two modes.  Each mode will be
explained individually.
.PP
Take the following command line:
.br
.B \tpagenera template.html page1.html siteA siteB page2.html
.\"
.SS Template parsing
First, slots inside
.B template.html
will be parsed and the template will be "gapped."  This operation happens only
once per compilation process.
.\"
.SS Single page mode
Given that each
.B pageN.html
is a regular file.  The first target, \fBpage1.html\fR, contains a bunch of
filler definitions among its lines, which will be extracted by
.B Pagenera
into a key-data store where slot names and fillers are linked.  Now, a stream
will be generated by "filling" every slot in the template with the
corresponding filler from the store, according to the way pasting is
configured (see \fBPaste Options\fR).  The generated stream will be written to
the output file.  The same stream-generation operation will be done for all
pages.
.\"
.SS Site mode
Given that each
.B siteX
is a directory.  First,
.B Pagenera
will crawl the first target, \fBsiteA\fR, looking what's inside.  In this
process, every regular file is recognized as a page, where directories are
recognized as sub-sites, which in turn will be recursively crawled.  For every
sub-site, as well as the parent site, an empty store is created.  When
.B Pagenera
faces a page whose name matches the
.I PAGENERA_GLOBAL_FILENAME
environment variable
(which can be set to any valid non-empty filename),
its filler definitions will be extracted into those site-level stores. A
sub-site will inherit its parent's store, and the sub-site's site-level
definitions will override those inherited from the parent.  Now, when compiling
pages, each page will inherit its site's store, which in turn can be overridden
by page's own definitions.  The same crawling operating will be done for the
other site targets.
.\"
.SS Paste Options
The paste routine is supplied with a built-in "prettifier" that affects the way
slots are filled depending on their respective placement in the template file,
as well as the content of the filler (or lack thereof).
.PP
Beforehand, these definitions need to be understood.
.TP
Inserted slot
A slot with non-indent text on the left, or any text on the right, of the line
in which the slot exists.
.TP
Indented slot
A slot in line with with leading whitespaces (tabs, spaces or both).
.TP
Unsatisfied slot
A slot that does not have a corresponding filler.
.TP
Empty filler
A filler that contains nothing (zero lines).
.TP
Multi-line filler
A filler that contains more than one line.
.PP
Now, the behaviour of the "prettifier" can be configured with the
.I PAGENERA_PASTE_OPTIONS
environment variable, which can contain an optional colon-separated combination
of the following options:
.TP
.B DELIMIT
When this option is set, an inserted slot will be have its output's leading
indent and trailing newlines replaced by a single space.
.TP
.B EMPTY
Normally, if a slot is unsatisfied, it will not be filled and thus will be seen
intact in the output. When this option is set, any unsatisfied slot will be
filled with an empty filler.
.TP
.B INDENT
When this option is set, an indented slot with a multi-line filler will have its
output indented likewise (except for the first line, which is already indented
in the template).
.TP
.B SHADOW
(TO BE IMPLEMENTED)
When this option is set, a slot with an empty filler will have its trailing
space deleted (if a the slot is entitled by spaces on both sides) or will
have its whole line deleted (in case the slot is not inserted at all).
.\"
.SS Notes
.PP
Targets (given in a command line) cannot affect each others' stores.
.PP
Crawling will happen in the order specified in the
.I PAGENERA_CRAWLING_ORDER
environment variable, which can be set to either
.B ASCEND
(ascending, default) or
.B DESCEND
(descending).  Sites' pages are always compiled before their subsites' ones.
.\"
.\"
.SH ENVIRONMENT
.B Pagenera
does not use a configuration file, it rather utilizes environment variables for
the purpose of modifying its own behaviour.  This allows for process-dependent
configuration in a (perhaps) more straight-forward way.
.PP
Following is a table of available environment variables associated with the
place they were documented within this manual:
.TP
.I PAGENERA_CRAWLING_ORDER
Explained in the
.B Notes
subsection in the
.B COMPILATION
section.
Default value:
.B \'ASCEND\'
.TP
.I PAGENERA_GLOBAL_FILENAME
Explained in the
.B Site Mode
subsection in the
.B COMPILATION
section.
Default value:
.B \'global\'
.TP
.I PAGENERA_OUTPUT_SUFFIX
Explained in the
.B PAGES AND SITES
section.
Default value:
.B \'.out\'
.TP
.I PAGENERA_PASTE_OPTIONS
Explained in the
.B Paste Options
subsection in the
.B COMPILATION
section.
Default value:
.B \'\'
.TP
.I PAGENERA_SLOT_REGEX
Explained in the
.B SLOTS
section.
Default value:
.B \'%[A-Z0-9\-]+%\'
.\"
.\"
.SH LOG
Within the compilation process,
.B Pagenera
sends log messages into the standard error file.  Except for the welcome
message, these log messages are tagged with the following tags:
.TP
.I NOTE
A mere notice message. Useful for debugging.
.TP
.I WARN
A violation of a rule that "SHOULD" be applied.
.TP
.I ERROR
A violation of a rule that "MUST" be applied within a specific target.
.B Pagenera
will skip that target and continue the compilation process.
.TP
.I FATAL
A violation of a rule that "MUST" be applied within the whole compilation
process.  In this case,
.B Pagenera
will halt before processing any kind of compilation happens.
.\"
.\"
.SH EXIT STATUS
When the compilation prcoess halts, it returns an exit status which can be
interpreted as the following:
.TP
0
Everything went alright.  No failed targets (i.e. No "\fIERRORS\fR").
.TP
1
Syntax error in the command line (a "FATAL").
.TP
2
An environment variable cannot be interpreted properly (a "\fIFATAL\fR").
.TP
3
Error happened while processing the template (a "\fIFATAL\fR").
.TP
4+
Number of failed target ("\fIERRORS\fR") plus three. If that number is actually
larger than 255,
.B Pagenera
will return 255 anyway.
.PP
It's advisable to rely on the log messages to detect issues rather than the exit
status.
.\"
.\"
.SH HOME
<https://codeberg.org/karam/pagenera>
.\"
.\"
.SH COPYRIGHT
Copyright (C) 2021 Karam Assany <karam.assany@disroot.org>
.PP
.B Pagenera
is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.
.PP
.B Pagenera
is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License for more details.
.PP
You should have received a copy of the GNU General Public License along with
\fBPagenera\fR.  If not, see <https://www.gnu.org/licenses/>.
